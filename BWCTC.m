function [BW, CTC] = BWCTC(M, N, K, S, Tm, Tn, Tr, Tc, Ti, Tj, GFLOPS, R, C)
Bin = inBW(S, Tn, Tr, Tc, Ti, Tj);
Bwght = wghtBW(Tm, Tn, Ti, Tj);
Bout = outBW(Tm, Tr, Tc);


alphain = (M * N * R * C * K * K) /  (Tm * Tn * Tr * Tc * Ti * Tj);
alphawght = alphain;
alphaOut = (M * R * C) / (Tm * Tr * Tc); 
%CTC = (N * K * K) / (2*(1 + ((N * K * K) / (Tr * Tc)) + ((N * K * K * (S*Tr+Ti-S)*(S*Tc+Tj-S))/(Tm*Tr*Tc*Ti*Tj))));
CTC = (2 * R * C * M * N * K * K ) / ((alphain * Bin) + (alphawght * Bwght) + (alphaOut * Bout));
BW = (GFLOPS) / CTC;
end

