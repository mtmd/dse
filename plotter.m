close all;
figure;
hold on;
plot([0 22.22],[0 100],'-red');
flag = 1;
for i = 1:40
    tmp = find (abs(mtmdCTC - i) < 0.001);
    for j = tmp
        plot(mtmdCTC(j),mtmdGFLOPS(j),'black.-', 'markersize', 10);
    end
end
oldJ = 1;
tmp = find ((mtmdGFLOPS - 89) > 0.0);
    for j = tmp
        if (mtmdCTC(j) <40 && (mtmdCTC(j) - floor(mtmdCTC(j)) < 0.1))
        oldJ = j;
        plot(mtmdCTC(j),mtmdGFLOPS(j),'black.-', 'markersize', 10);
        end
    end