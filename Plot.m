tic
clc;
clear;
f = 100000000;
numOfLayers = 5;
numOfPEs = 480; % ==> Speedup: 2500;
szParaConv = 10;
numOfPEs = numOfPEs / szParaConv;
totalBW = 4.5; % ==> Speedup: GBps 9;
BDRAMCapacity = (4746240); %DRAM Capacity Byte
%N = [3, 96, 256, 384, 384]; % number of input feature maps in different layers
%M = [96, 256, 384, 384, 256]; % number of output feature maps in different layers
N = [3, 48, 128, 192, 192]; % number of input feature maps in different layers
M = [48, 128, 192, 192, 128]; % number of output feature maps in different layers
R = [55, 27, 13, 13, 13]; % number of rows in output feature map
C = [55, 27, 13, 13, 13]; % number of cols in output feature map
K = [11, 5, 3, 3, 3]; % kernel sizes in different layers
S = [4, 1, 1, 1, 1];     % Strides in different kernels

assert(size(N, 2) == numOfLayers, 'Invalid size for N');
assert(size(M, 2) == numOfLayers, 'Invalid size for M');
assert(size(R, 2) == numOfLayers, 'Invalid size for R');
assert(size(C, 2) == numOfLayers, 'Invalid size for C');
assert(size(K, 2) == numOfLayers, 'Invalid size for K');
assert(size(S, 2) == numOfLayers, 'Invalid size for S');

bestGFLOPS = zeros (1, numOfLayers);
bestTm = zeros (1, numOfLayers);
bestTn = zeros (1, numOfLayers);
bestTr = zeros (1, numOfLayers);
bestTc = zeros (1, numOfLayers);
bestTi = zeros (1, numOfLayers);
bestTj = zeros (1, numOfLayers);
bestCycle = zeros (1, numOfLayers);
%for l = 5:5
plotCount = 0;
for l = 1:1
    figure;
    hold on;
    plot([0 22.22],[0 100],'-red');
    %plot([0 55.55],[0 250],'-red');
    title('Design Space Layer 1');
    xlabel('Computation to Communication Ratio');
    ylabel('GFLOPS');
    tmpCTC = 0;
    for Tm = 1:M(l)
        for Tn = 1:N(l)
            if ((Tm * Tn <= numOfPEs))
                for Tr = 1:R(l)
                    for Tc = 1:C(l)
                        for Ti = 1:K(l)
                            for Tj = 1:K(l)
                                %if Ti * Tj <= szParaConv
                                    if (memCheck(S(l), Tm, Tn, Tr, Tc, Ti, Tj, BDRAMCapacity))
                                        GFLOPS = calcGFLOPS(M(l), N(l), K(l), Tm, Tn, Ti, Tj,  f, szParaConv);                                        
                                        %if bestGFLOPS(l) <= GFLOPS
                                        if (1 == 1)
                                            [BW, CTC] = BWCTC(M(l), N(l), K(l), S(l), Tm, Tn,  Tr, Tc, Ti, Tj, GFLOPS, R(l), C(l));
                                            plotCount = plotCount + 1;
                                            mtmdCTC(plotCount) = CTC;
                                            mtmdGFLOPS(plotCount) = GFLOPS;
                                            if BW < totalBW    
                                                %if (mod(plotCount, 10) == 0)
                                                    %plot(CTC,GFLOPS,'black.-', 'markersize', 7);
                                                %end
                                                %if (bestGFLOPS(l) == GFLOPS && CTC > tmpCTC)
                                                cycles = (ceil(M(l)/ Tm) * ceil(N(l) / Tn) * (R(l)) * (C(l))) * ceil(K(l) / Ti) * ceil (K(l) / Tj) * ceil((Ti * Tj) / szParaConv);
                                                if (bestGFLOPS(l) == GFLOPS && bestCycle (l) > cycles)
                                                    bestGFLOPS(l) = GFLOPS;
                                                    bestTm (l) = Tm;
                                                    bestTn (l) = Tn;
                                                    bestTr (l) = Tr;
                                                    bestTc (l) = Tc;
                                                    bestTi (l) = Ti;
                                                    bestTj (l) = Tj;
                                                    bestCycle (l) = cycles;
                                                    tmpCTC = CTC;
                                                    tmpGFLOPS = GFLOPS;
                                                elseif (bestGFLOPS(l) < GFLOPS)
                                                    bestGFLOPS(l) = GFLOPS;
                                                    bestTm (l) = Tm;
                                                    bestTn (l) = Tn;
                                                    bestTr (l) = Tr;
                                                    bestTc (l) = Tc;
                                                    bestTi (l) = Ti;
                                                    bestTj (l) = Tj;                                                    
                                                    bestCycle (l) = cycles;
                                                    tmpCTC = CTC;
                                                    tmpGFLOPS = GFLOPS;
                                                end
                                            end
                                        end
                                    end
                                %end
                            end
                        end
                    end
                end
            end
        end
    end
end
best = [bestTm; bestTn; bestTr; bestTc; bestTi; bestTj; bestCycle; bestGFLOPS];
toc