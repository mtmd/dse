function [ GFLOPS ] = calcGFLOPS(M, N, K, Tm, Tn, Ti, Tj,  f, Tk)
%compRoof = (2 * M * N * K * K) / (ceil(M / Tm) * ceil(N / Tn) * ceil((K * K)/(Ti * Tj)));
compRoof = (2 * M * N * K * K) / (ceil(M / Tm) * ceil(N / Tn) * ceil(K / Ti) * ceil(K / Tj) * ceil((Ti * Tj) / Tk));
GFLOPS = (compRoof * f) / 1000000000;
end

