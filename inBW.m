function [ Bin ] = inBW(S, Tn, Tr, Tc, Ti, Tj)
Bin = 4 * Tn * (S * Tr + Ti - S) * (S * Tc + Tj - S);
end

