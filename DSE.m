tic
clc;
clear;
f = 100000000;
numOfLayers = 5;
numOfPEs = 480; %2500
totalBW = 4.5; % GBps 9
BDRAMCapacity = (4746240); %DRAM Capacity Byte
%N = [3, 96, 256, 384, 384]; % number of input feature maps in different layers
%M = [96, 256, 384, 384, 256]; % number of output feature maps in different layers
N = [3, 48, 128, 192, 192]; % number of input feature maps in different layers
M = [48, 128, 192, 192, 128]; % number of output feature maps in different layers
R = [55, 27, 13, 13, 13]; % number of rows in output feature map
C = [55, 27, 13, 13, 13]; % number of cols in output feature map
K = [11, 5, 3, 3, 3]; % kernel sizes in different layers
S = [4, 1, 1, 1, 1];     % Strides in different kernels

assert(size(N, 2) == numOfLayers, 'Invalid size for N');
assert(size(M, 2) == numOfLayers, 'Invalid size for M');
assert(size(R, 2) == numOfLayers, 'Invalid size for R');
assert(size(C, 2) == numOfLayers, 'Invalid size for C');
assert(size(K, 2) == numOfLayers, 'Invalid size for K');
assert(size(S, 2) == numOfLayers, 'Invalid size for S');

bestGFLOPS = zeros (1, numOfLayers);
bestTm = zeros (1, numOfLayers);
bestTn = zeros (1, numOfLayers);
bestTr = zeros (1, numOfLayers);
bestTc = zeros (1, numOfLayers);
bestCycle = zeros (1, numOfLayers);
BTm = [48, 20, 96, 95, 32];
BTn = [3, 24, 5, 5, 15];


parfor l = 1:numOfLayers
    %figure
    %hold on;
    %plot([0 64.516129],[0 100],'-')
    tmpCTC = 0;
    for Tm = 1:M(l)
        for Tn = 1:N(l)
            if ((Tm * Tn <= numOfPEs))
                for Tr = 1:R(l)
                    for Tc = 1:C(l)
                        Bin = Tn * (S(l) * Tr + K(l) - S(l)) * (S(l) * Tc + K(l) - S(l));
                        Bwght = Tm * Tn * K(l) * K(l);
                        Bout = Tm * Tr * Tc;
                        Bin = Bin * 4;
                        Bwght = Bwght * 4;
                        Bout = Bout * 4;
                        if (Bin + Bwght + Bout <= BDRAMCapacity)
                             compRoof = (2 * M(l) * N(l)) / (ceil(M(l)/ Tm) * ceil(N(l) / Tn));
                             GFLOPS = (compRoof * f) / 1000000000;
                             if bestGFLOPS(l) <= GFLOPS
                                 CTC = (2 * N(l) * K(l) * K(l)) / (4*(1 + ((N(l) * K(l) * K(l)) / (Tr * Tc)) + ((N(l)*(S(l)*Tr+K(l)-S(l))*(S(l)*Tc+K(l)-S(l)))/(Tm*Tr*Tc))));
                                 BW = (GFLOPS) / CTC;
                                 %plot(CTC,GFLOPS,'black.-', 'markersize', 10);
                                 if (Tm == BTm(l) && Tn == BTn(l) ) %&& BW < 1.55
                                     %  plot(CTC,GFLOPS,'red.-', 'markersize', 20);
                                 end
                                 if BW < totalBW
                                     if (bestGFLOPS(l) == GFLOPS && CTC > tmpCTC)
                                        bestGFLOPS(l) = GFLOPS;
                                        bestTm (l) = Tm;
                                        bestTn (l) = Tn;
                                        bestTr (l) = Tr;
                                        bestTc (l) = Tc;
                                        bestCycle (l) = (ceil(M(l)/ Tm) * ceil(N(l) / Tn) * (R(l)) * (C(l)) * K(l) * K(l));
                                        tmpCTC = CTC;
                                        tmpGFLOPS = GFLOPS;
                                     elseif (bestGFLOPS(l) < GFLOPS) 
                                        bestGFLOPS(l) = GFLOPS;
                                        bestTm (l) = Tm;
                                        bestTn (l) = Tn;
                                        bestTr (l) = Tr;
                                        bestTc (l) = Tc;
                                        bestCycle (l) = (ceil(M(l)/ Tm) * ceil(N(l) / Tn) * (R(l)) * (C(l)) * K(l) * K(l));
                                        tmpCTC = CTC;
                                        tmpGFLOPS = GFLOPS;
                                     end
                                 end
                             end
                        end 
                    end
                end
            end
        end
    end
end
%plot(tmpCTC,tmpGFLOPS,'green.-', 'markersize', 10);
best = [bestTm; bestTn;bestTr;bestTc;bestCycle;bestGFLOPS];
toc