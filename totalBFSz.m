function [BFSz] = totalBFSz(S, Tm, Tn, Tr, Tc, Ti, Tj)
Bin = inBW(S, Tn, Tr, Tc, Ti, Tj);
Bwght = wghtBW(Tm, Tn, Ti, Tj);
Bout = outBW(Tm, Tr, Tc);
BFSz = Bin + Bwght + Bout;
end

